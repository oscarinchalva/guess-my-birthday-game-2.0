from random import randint

name = input("Hi! What is your name?")

# First guess
for guess in range (1, 6):
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2022)
    print(f"Guess {guess} :  {name}  were you born in {guess_month} / {guess_year} ?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif guess == 5:
        print("I have other things to do. Goodbye.")
    else:
        print("Drat! Lemme try again!")
